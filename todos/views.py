from django.shortcuts import render, redirect, get_object_or_404
from todos.models import TodoList, TodoItem 
from todos.forms import TodoListForm
from django.contrib.auth.decorators import login_required


def todo_list_list(request):
    todos = TodoList.objects.all()
    context = {
        "todo_list_list": todos,
    }
    return render(request, "todos/todos.html", context)

def todo_list_detail(request, id):
    list = TodoList.objects.get(id=id) #model name as arg?
    context = {
        "list_instance": list
    }
    return render(request, "todos/todo_list_detail.html", context)

def todo_list_create(request):
  if request.method == "POST":
    form = TodoListForm(request.POST)
    if form.is_valid():
      # To redirect to the detail view of the model, use this:
      list = form.save()
      return redirect("todo_list_detail", id=list.id)

      # To add something to the model, like setting a user,
      # use something like this:
      #
      # model_instance = form.save(commit=False)
      # model_instance.user = request.user
      # model_instance.save()
      # return redirect("detail_url", id=model_instance.id)
  else:
    form = TodoListForm()

  context = {
    "form": form
  }
  return render(request, "todos/todo_list_create.html", context)

def todo_list_update(request, id):
    list = TodoList.objects.get(id=id)
    if request.method == "POST":
      form = TodoListForm(request.POST, instance=list)
      if form.is_valid():
         list = form.save()
         return redirect("todo_list_detail", id=list.id)
    else:
      form = TodoListForm(instance=list)
    context = {
       "form": form,
    }
    return render(request, "todos/to_do_list_update.html", context)
      
def todo_list_delete(request, id):
   if request.method == "POST":
      list = TodoList.objects.get(id=id)
      list.delete()
      return redirect("todo_list_list")
   return render (request, "todos/to_do_list_delete.html")
