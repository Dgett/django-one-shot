from django.forms import ModelForm
from todos.models import TodoItem, TodoList



class TodoListForm(ModelForm):
    class Meta:
        model = TodoList          # Step 3
        fields = [              # Step 4
            "name"                   # Step 4     # Step 4
        ]
        
class TodoItemForm(ModelForm):
    class Meta:
        model = TodoItem          # Step 3
        fields = "__all__"             # Step 4
                                  # Step 
